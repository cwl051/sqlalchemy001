class Person():
    def __init__(self,first,last,gender):
        self.first = first
        self.last = last
        self.gender = self.getGender(gender)

    def getGender(self,gender):
        if str(gender).lower() in ["m", "male", "masculin", "man", "mannelijk", "mann", "homme", "boy", "garçon", "jongen"]:
            return "M"
        else:
            return "F"

    def __str__(self):
        return(self.first + " " + self.last + ", " + self.gender)

"""
#testing class Person
johnDoe = Person("John","Doe","meisje")
print(johnDoe)
"""
