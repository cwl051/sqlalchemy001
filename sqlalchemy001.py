import sqlalchemy
import sqlite3

import Person as p

print("SqlAlchemy version: ", sqlalchemy.__version__)
print("Sqlite version: ", sqlite3.sqlite_version_info)

cwl = p.Person('Christophe','Willaert','M')
print(cwl)
